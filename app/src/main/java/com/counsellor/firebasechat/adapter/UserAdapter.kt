package com.counsellor.firebasechat.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.counsellor.firebasechat.R
import com.counsellor.firebasechat.model.ChatUser
import com.counsellor.firebasechat.ui.chat.ChatActivity
import com.skysys.plugh.utils.Util
import de.hdodenhof.circleimageview.CircleImageView

class UserAdapter(private val context: Context, private var userList: ArrayList<ChatUser>) :
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = userList[position]
        holder.txtUserName.text = user.firstname
        holder.txtEndTime.text = user.createdAt?.let {
            Util.convertDate("yyyy-MM-dd HH:mm:ss",
                it,"dd-MM-yyyy h:MM a")
        }
//        Glide.with(context).load(user.profileImage).placeholder(R.drawable.profile_image).into(holder.imgUser)

        holder.layoutUser.setOnClickListener {

            val intent = Intent(context, ChatActivity::class.java)
                .putExtra("userData",userList[position])
            context.startActivity(intent)
        }
    }

    fun update(userList: ArrayList<ChatUser>) {
        this.userList = userList
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val txtUserName:TextView = view.findViewById(R.id.userName)
        val txtEndTime:TextView = view.findViewById(R.id.txtEndTime)
        val imgUser:CircleImageView = view.findViewById(R.id.userImage)
        val layoutUser:LinearLayout = view.findViewById(R.id.layoutUser)
    }
}