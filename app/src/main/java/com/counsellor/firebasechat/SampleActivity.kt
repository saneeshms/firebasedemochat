package com.counsellor.firebasechat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class SampleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)

//        var a = A()
//        Singleton.printVarName()
//
//        println(aVar)
//        println(aVar)

        var str:String? = null

        var id = 0

        str?.let {
            id++
        } ?: run {
            id++
        }

        println("dataID = $id")

    }

    val aVar by lazy {
        println("I am computing this value")
        "Hola"
    }

    //..................................

    open class A {
        open fun printVarName() {
            print("I am in class printVarName")
        }

        init {
            println("I am in init of A")
        }
    }

    object Singleton : A() {
        init {
            println("Singleton class invoked.")
        }

        var variableName = "I am Var"
        override fun printVarName() {
            println(variableName)
        }
    }


}