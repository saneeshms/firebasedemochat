package com.skysys.plugh.utils

import android.content.ContentResolver
import android.content.Context
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.counsellor.firebasechat.Constants.Constants
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

object Util {

    var mContext: Context ? = null

    fun getDateTime() : String
    {
        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())
    }

    fun convertDate(fromFormat:String,date:String,toFormat:String): String {
        var fromDate = SimpleDateFormat(fromFormat).parse(date)
        return SimpleDateFormat(toFormat).format(fromDate)
    }

    fun getContext(): Context{
        return mContext!!
    }

    var r: Ringtone? = null

    @RequiresApi(Build.VERSION_CODES.P)
    fun playNotificationSound(applicationContext: Context) {
        try {
            val alarmSound = Uri.parse(
                ContentResolver.SCHEME_ANDROID_RESOURCE
                        + "://" + applicationContext.getPackageName() + "/raw/sweet_text"
            )
            r = RingtoneManager.getRingtone(applicationContext, alarmSound)
            r!!.play()
            r!!.isLooping = true

            val th = Thread {
                try {
                    Thread.sleep(15000)
                    if (r!!.isPlaying()){
                        r!!.stop()
                    } // for stopping the ringtone
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
            th.start()


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun stopRingTone()
    {
        Log.d(  Constants.TAG_CHECK, "stopRingTone: start ")

        if (r != null && r!!.isPlaying) {
            Log.d(  Constants.TAG_CHECK, "stopRingTone: ")
            r!!.stop()
        }
    }

}