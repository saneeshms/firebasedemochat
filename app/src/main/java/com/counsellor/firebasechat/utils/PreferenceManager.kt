package com.skysys.plugh.utils

import android.content.Context
import android.content.SharedPreferences

object PreferenceManager {

    var mEditor: SharedPreferences.Editor? = null
    var mPref: SharedPreferences? = null

    fun getSession(context: Context) {
        mPref = context.getSharedPreferences("PlugH", Context.MODE_PRIVATE)
        mEditor = mPref?.edit()
    }

     fun setIsrated(data: Boolean) {
       mEditor?.putBoolean("isRated", false)
        mEditor?.apply()
    }

    fun isRated(): Boolean {
        return mPref!!.getBoolean("isRated", false)
    }

    fun getUserId() : String {
        return mPref!!.getString("user_id", "")!!
    }

    fun setUserId(userId: String) {
        mEditor!!.putString("user_id", userId).commit()
    }

    fun getUserName() : String {
        return mPref!!.getString("user_name", "")!!
    }

    fun setUserName(name: String) {
        mEditor!!.putString("user_name", name).commit()
    }

    fun getToken() : String {
        return mPref!!.getString("token", "")!!
    }

    fun setToken(userId: String) {
        mEditor!!.putString("token", userId).commit()
    }

    fun isOnChatScreen() : Boolean {
        return mPref!!.getBoolean("chat_screen", false)
    }

    fun setOnChatScreen(flag: Boolean) {
        mEditor!!.putBoolean("chat_screen", flag).commit()
    }
}