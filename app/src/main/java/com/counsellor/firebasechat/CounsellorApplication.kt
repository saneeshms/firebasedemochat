package com.counsellor.firebasechat

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CounsellorApplication : Application() {

}