package com.counsellor.firebasechat.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Counsellor(var name:String ? = null,
                      var primary_contact :String ? = null,
                      @SerializedName("chat_id") var chatId :String ? = null,
                      @SerializedName("firebase_user_id") var firebaseUserId : String ? = null,
                      @SerializedName("counsellorId") var counsellorId : String ? = null,
                      @SerializedName("gender") var gender : String ? = null,
                      @SerializedName("status") var status : String ? = null,
                      @SerializedName("educational_qualification") var qualification : String ? = null,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()) {
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, p1: Int) {
        parcel.writeString(name)
        parcel.writeString(primary_contact)
        parcel.writeString(chatId)
        parcel.writeString(firebaseUserId)
        parcel.writeString(counsellorId)
        parcel.writeString(gender)
        parcel.writeString(status)
        parcel.writeString(qualification)
    }

    companion object CREATOR : Parcelable.Creator<Counsellor> {
        override fun createFromParcel(parcel: Parcel): Counsellor {
            return Counsellor(parcel)
        }

        override fun newArray(size: Int): Array<Counsellor?> {
            return arrayOfNulls(size)
        }
    }
}