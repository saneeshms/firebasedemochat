package com.counsellor.firebasechat.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ChatUser(
    var firstname:String ? = null,
    var middlename:String ? = null,
    var lastname:String ? = null,
    var profileImage:String ? = null,
    @SerializedName("user_id") var userId: String? = null,
    @SerializedName("firebase_userid") var firebaseUserId: String? = null,
    @SerializedName("counsellor_id") var counsellorId: String? = null,
    @SerializedName("firebase_counsellor_id") var firebaseCounsellorId: String? = null,
    @SerializedName("chat_id") var chatId: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("isChatEnabled") var isChatEnabled: String? = null
                    ) : Parcelable
{
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(firstname)
        parcel.writeString(middlename)
        parcel.writeString(lastname)
        parcel.writeString(profileImage)
        parcel.writeString(userId)
        parcel.writeString(firebaseUserId)
        parcel.writeString(counsellorId)
        parcel.writeString(firebaseCounsellorId)
        parcel.writeString(chatId)
        parcel.writeString(createdAt)
        parcel.writeString(isChatEnabled)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChatUser> {
        override fun createFromParcel(parcel: Parcel): ChatUser {
            return ChatUser(parcel)
        }

        override fun newArray(size: Int): Array<ChatUser?> {
            return arrayOfNulls(size)
        }
    }

}