package com.counsellor.firebasechat.model

import com.google.gson.annotations.SerializedName

data class User(var token:String,@SerializedName("user_id") var userId :String,
                @SerializedName("counsellor_nickname") var counsellor_nickname :String) {
}