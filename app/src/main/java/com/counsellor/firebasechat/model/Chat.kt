package com.skysys.plugh.eap.model

data class Chat(var senderId:String = "", var receiverId:String = "", var message:String = "")