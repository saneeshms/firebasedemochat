package com.counsellor.firebasechat.model

data class PushNotification(
    var data: NotificationData,
    var to:String
)