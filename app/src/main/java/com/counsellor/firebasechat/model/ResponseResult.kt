package com.counsellor.firebasechat.model

data class ResponseResult<T>(var status : String,var data : T , var code : String) {
}