package com.counsellor.firebasechat.model

data class NotificationData(
    var title:String,
    var message:String,
    var name:String,
    var chatId:String,
    var firebaseCounsellorId:String,
    var counsellorId:String,
)