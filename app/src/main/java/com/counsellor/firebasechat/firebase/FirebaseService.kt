package com.counsellor.firebasechat.firebase

import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.content.*
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.counsellor.firebasechat.Constants.Constants
import com.counsellor.firebasechat.R
import com.counsellor.firebasechat.model.ChatUser
import com.counsellor.firebasechat.ui.chat.ChatActivity
import com.skysys.plugh.utils.PreferenceManager
import com.skysys.plugh.utils.Util
import kotlin.random.Random


class FirebaseService : FirebaseMessagingService() {

    val CHANNEL_ID = "my_notification_channel"
    companion object{
        var sharedPref:SharedPreferences? = null

        var token:String?
        get(){
            return sharedPref?.getString("token","")
        }
        set(value){
            sharedPref?.edit()?.putString("token",value)?.apply()
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        token = p0
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        PreferenceManager.getSession(applicationContext)
        Log.d(Constants.TAG_CHECK, "onMessageReceived: "+PreferenceManager.isOnChatScreen())

        if (PreferenceManager.isOnChatScreen())
            return

        var chatUser : ChatUser = ChatUser()
        chatUser.firstname = p0.data["title"]
        chatUser.chatId = p0.data["chatId"]
        chatUser.firebaseUserId = p0.data["firebaseUserId"]
        chatUser.userId = p0.data["userId"]

        val intent = Intent(this, ChatActivity::class.java)
       intent.putExtra("userData",chatUser)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = Random.nextInt()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            createNotificationChannel(notificationManager)
        }

        val receiveCallAction = Intent(applicationContext, OnCancelBroadcastReceiver::class.java)
        receiveCallAction.putExtra(
            "ConstantApp.CHAT_RECEIVE",
            "ConstantApp.CHAT_RECEIVE"
        )

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this,0,intent, FLAG_IMMUTABLE)
        val notification = NotificationCompat.Builder(this,CHANNEL_ID)
            .setContentTitle(p0.data["title"])
            .setContentText(p0.data["message"])
            .setSmallIcon(R.drawable.ic_baseline_notifications_24)
            .setAutoCancel(true)
            .setOngoing(false)
        //    .setSound(alarmSound)
            .setContentIntent(pendingIntent)
            .build()

        val onDismissPendingIntent =
            PendingIntent.getBroadcast(this.applicationContext, 0, receiveCallAction, FLAG_IMMUTABLE)
        notification.deleteIntent = onDismissPendingIntent

        notificationManager.notify(1234, notification)
//
//        var incomingCallNotification: Notification? = null
//        incomingCallNotification = notification.build()
//        startForeground(1,incomingCallNotification)

        if(isAppIsInBackground(applicationContext)){
            Util.stopRingTone()
            Util.playNotificationSound(applicationContext)
        }

    }

    class OnCancelBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d(  Constants.TAG_CHECK, "OnCancelBroadcastReceiver: start ")
            Util.stopRingTone()
        }
    }

    private fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo!!.packageName == context.packageName) {
                isInBackground = false
            }
        }
        return isInBackground
    }



    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(notificationManager: NotificationManager){

        val alarmSound: Uri = Uri.parse(
            ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + packageName + "/raw/sweet_text"
        )

        val audioAttributes = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()


        val channelName = "ChannelFirebaseChat"
        val channel = NotificationChannel(CHANNEL_ID,channelName,IMPORTANCE_HIGH).apply {
            description="MY FIREBASE CHAT DESCRIPTION"
            enableLights(true)
            lightColor = Color.WHITE
           // setSound(alarmSound, audioAttributes)
        }
        notificationManager.createNotificationChannel(channel)
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
           Util.stopRingTone()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}