package com.skysys.plugh.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    val status : MutableLiveData<String> = MutableLiveData()
    val _status : LiveData<String>
        get() = status

    val loading : MutableLiveData<Boolean> = MutableLiveData()
    val _loading : LiveData<Boolean>
        get() = loading


}