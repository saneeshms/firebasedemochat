package com.skysys.plugh.common

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import javax.inject.Inject

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    fun showToast(context:Context,message:String)
    {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }

}