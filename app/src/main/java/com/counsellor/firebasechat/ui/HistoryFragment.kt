package com.counsellor.firebasechat.ui

import android.content.ContentValues
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging
import com.counsellor.firebasechat.adapter.UserAdapter
import com.counsellor.firebasechat.firebase.FirebaseService
import com.counsellor.firebasechat.model.ChatUser
import com.counsellor.firebasechat.model.Counsellor
import com.counsellor.firebasechat.R
import com.skysys.plugh.eap.chat.ChatViewModel
import com.skysys.plugh.utils.PreferenceManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_users.*
import kotlinx.android.synthetic.main.fragment_active.*
import kotlinx.android.synthetic.main.fragment_history.*
import javax.inject.Inject

@AndroidEntryPoint
class HistoryFragment : Fragment() {

    var userList = ArrayList<ChatUser>()

    @Inject
    lateinit var viewModel : ChatViewModel
    lateinit var userAdapter : UserAdapter
    lateinit var counsellor: Counsellor
    lateinit var useridFireBase : String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        FirebaseService.sharedPref = requireContext().getSharedPreferences("sharedPref",Context.MODE_PRIVATE)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(ContentValues.TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            FirebaseService.token = task.result
        })

        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        usersHistoryRecyclerView.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        initViews()
        initControls()
    }

    private fun initControls() {

        val firebase: FirebaseUser = FirebaseAuth.getInstance().currentUser!!
        useridFireBase = firebase.uid

    }

    override fun onResume() {
        super.onResume()

        var reqdata :HashMap<String,String> = hashMapOf()
        reqdata["user_id"] = PreferenceManager.getUserId()
        reqdata["list_type"] = "history"
        viewModel.fetchUsersList(reqdata)

        viewModel._userListData.observe(viewLifecycleOwner){
            userList = it as ArrayList<ChatUser>
            userAdapter.update(userList)
        }

        viewModel._loading.observe(viewLifecycleOwner) {
            if (it)
                aviLoading.show()
            else
                aviLoading.hide()
        }

    }

    private fun initViews() {

        userAdapter = UserAdapter(requireContext(), userList)
        usersHistoryRecyclerView.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        usersHistoryRecyclerView.adapter = userAdapter

        FirebaseService.sharedPref = requireContext().getSharedPreferences("sharedPref",Context.MODE_PRIVATE)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(ContentValues.TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            FirebaseService.token = task.result
        })


    }

    fun getUsersList() {

        val firebase: FirebaseUser = FirebaseAuth.getInstance().currentUser!!

        var userid = firebase.uid
        FirebaseMessaging.getInstance().subscribeToTopic("/topics/$userid")


        val databaseReference: DatabaseReference =
            FirebaseDatabase.getInstance().getReference("Users")
//        val dbReference : DatabaseReference = databaseReference.child("org_id")
//        dbReference.addListenerForSingleValueEvent(valueEventListener)

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()
//                val currentUser = snapshot.getValue(User::class.java)
//                if (currentUser!!.profileImage == ""){
//                    imgProfile.setImageResource(R.drawable.profile_image)
//                }else{
//                    Glide.with(this@UsersActivity).load(currentUser.profileImage).into(imgProfile)
//                }

                for (dataSnapShot: DataSnapshot in snapshot.children) {
                    val user = dataSnapShot.getValue(ChatUser::class.java)

                    if (user!!.userId != userid) {
                        userList.add(user!!)
                    }
                }
                val userAdapter = activity?.let { UserAdapter(it, userList) }

                usersHistoryRecyclerView.adapter = userAdapter
            }

        })
    }

    var valueEventListener: ValueEventListener = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            if (dataSnapshot.child("org_id").getValue(String::class.java) == "1111") {
                Log.d("WEWEW", "onDataChange: ")
            } else if (dataSnapshot.child("userType")
                    .getValue(String::class.java) == "Venue Owner"
            ) {

            }
        }

        override fun onCancelled(databaseError: DatabaseError) {

        }
    }

}