package com.skysys.plugh.ui.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.counsellor.firebasechat.Constants.Constants
import com.skysys.plugh.common.BaseViewModel
import com.skysys.plugh.utils.PreferenceManager
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(var repository: LoginRepository) : BaseViewModel() {

    var message: MutableLiveData<String> = MutableLiveData()
    
    fun userLogin(reqBody: HashMap<String, String>, context: Context) {

        loading.value = true
        viewModelScope.launch {
            var responseData = repository.Login(reqBody).body()
            if(responseData == null)
                status.value = "Poor network. Please try again !"
            else if (responseData!!.code.contentEquals("200")) {

                Log.d(Constants.TAG_CHECK, "userLogin: "+responseData.data.userId)
                PreferenceManager.setToken(responseData.data.token)
                PreferenceManager.setUserId(responseData.data.userId)
                PreferenceManager.setUserName(responseData.data.counsellor_nickname)

                status.value = "success"
            } else {
                status.value = responseData.status
                if (responseData.code.equals(100)) {
                    message.value = "Login failed. Try again"
                } else if (responseData.code.equals(500)) {
                    message.value = "token creation failed"
                } else if (responseData.code.equals(401)) {
                    message.value = "Token expired"
                } else if (responseData.code.equals(403)) {
                    message.value = "Invalid token"
                } else if (responseData.code.equals(404)) {
                    message.value = "Token not found"
                }
            }
            loading.value = false
        }
    }


    fun userLogout(reqBody: HashMap<String, String>, context: Context) {

        loading.value = true
        viewModelScope.launch {
            var responseData = repository.userLogout(reqBody).body()
            if(responseData == null)
                status.value = "Poor network. Please try again !"
            else if (responseData!!.code.contentEquals("200")) {
                status.value = "success"
            } else {
                status.value = responseData.status
            }
            loading.value = false
        }
    }

}