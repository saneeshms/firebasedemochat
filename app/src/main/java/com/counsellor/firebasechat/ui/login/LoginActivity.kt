package com.counsellor.firebasechat.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.counsellor.firebasechat.Constants.Constants
import com.counsellor.firebasechat.R
import com.counsellor.firebasechat.databinding.ActivityLoginBinding
import com.counsellor.firebasechat.ui.ConversationActivity
import com.skysys.plugh.common.BaseActivity
import com.skysys.plugh.ui.login.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : BaseActivity() {

    private var auth: FirebaseAuth? = null
    private lateinit var databaseReference: DatabaseReference

    lateinit var data:String

    @Inject
    lateinit var loginViewModel: LoginViewModel
    lateinit var bindings: ActivityLoginBinding

    var firebaseUserId = ""
    var email = ""
    var password = ""
    var isPasswordOn = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindings = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(bindings.root)

        initViews()
        initControls()
    }

    private fun initControls() {

        auth = FirebaseAuth.getInstance()

        bindings.layoutLogIn.setOnClickListener {

            //firebase login
            email = bindings.tvUsername.text.toString()
            password = bindings.tvPassword.text.toString()

            if(email.isNullOrEmpty()) {
                showToast(
                    this,
                    "Please enter Email address"
                )
                return@setOnClickListener
            }else if(password.isNullOrEmpty())
            {
                showToast(
                    this,
                    "Please enter password"
                )
                return@setOnClickListener
            }else {

                checkUserLogin(email, password)
            }

        }

        loginViewModel.status.observe(this) {
            if (loginViewModel.status.value!!.equals("success")) {
                val intent = Intent(this, ConversationActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }

        }

        aviLoading.hide()
        loginViewModel._loading.observe(this) {
            if (it)
                bindings.aviLoading.show()
            else
                bindings.aviLoading.hide()
        }

        bindings.imgPassword.setOnClickListener {
            if(isPasswordOn)
            {
                bindings.imgPassword.setImageResource(R.drawable.ic_outline_visibility_off_24)
                bindings.tvPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                bindings.tvPassword.setSelection(bindings.tvPassword.text.length)

            }else
            {
                bindings.imgPassword.setImageResource(R.drawable.ic_baseline_visibility_24_on)
                bindings.tvPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                bindings.tvPassword.setSelection(bindings.tvPassword.text.length)
            }
            isPasswordOn = !isPasswordOn


        }

    }

    private fun loginApi() {

        val hashMap: HashMap<String, String> = hashMapOf()
        hashMap.put("email_id", bindings.tvUsername.text.toString())
        hashMap.put("password", bindings.tvPassword.text.toString())
        hashMap.put("firebase_counsellor_id", firebaseUserId)

        Log.d(Constants.TAG_CHECK, "userLogin : " + hashMap)
        loginViewModel.userLogin(hashMap, this)
    }

    private fun initViews() {


    }

    private fun checkUserLogin(email: String, password: String): Boolean {

        auth!!.signInWithEmailAndPassword(email, "123456").addOnCompleteListener {
            if (it.isSuccessful) {
                Log.d(Constants.TAG_CHECK, "signInWithEmailAndPassword SUCCESS ")
                val user: FirebaseUser? = auth!!.currentUser
                firebaseUserId = user!!.uid
                loginApi()

            } else {
                Log.d(Constants.TAG_CHECK, "signInWithEmailAndPassword Email or password invalid ")
                registerUser()
            }
        }

        return false
    }

    private fun registerUser() {

        auth!!.createUserWithEmailAndPassword(email, "123456")
            .addOnFailureListener {
                Log.d(Constants.TAG_CHECK, "createUserWithEmailAndPassword Failed ")
                showToast(this, "Create User WithEmailAndPassword Failed")
            }
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val user: FirebaseUser? = auth!!.currentUser
                    firebaseUserId = user!!.uid

                    databaseReference =
                        FirebaseDatabase.getInstance().getReference("Users").child(firebaseUserId)

                    val hashMap: HashMap<String, String> = HashMap()
                    hashMap.put("userId", firebaseUserId)
                    hashMap.put("userType", "counsellor")
                    hashMap.put("email", email)
                    hashMap.put("profileImage", "")
                    hashMap.put("org_id", "22455")

                    databaseReference.setValue(hashMap).addOnFailureListener {
                        Log.d(Constants.TAG_CHECK, "createUserWithEmailAndPassword Failed " + it)
                        showToast(this, "Create User WithEmailAndPassword Failed")
                    }.addOnCompleteListener() {
                        if (it.isSuccessful) {
                            Log.d(Constants.TAG_CHECK, "createUserWithEmailAndPassword SUCCESS ")
                            loginApi()
                        }
                    }
                }
            }
    }


}