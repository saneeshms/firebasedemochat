package com.counsellor.firebasechat.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.counsellor.firebasechat.R
import com.counsellor.firebasechat.ui.chat.UsersActivity
import com.counsellor.firebasechat.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var databaseReference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        auth = FirebaseAuth.getInstance()

        btnSignUp.setOnClickListener {
            val userName = etName.text.toString()
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()
            val confirmPassword = etConfirmPassword.text.toString()
//
//            if (TextUtils.isEmpty(userName)) {
//                Toast.makeText(applicationContext, "username is required", Toast.LENGTH_SHORT)
//                    .show()
//            }
//            if (TextUtils.isEmpty(email)) {
//                Toast.makeText(applicationContext, "email is required", Toast.LENGTH_SHORT).show()
//            }
//
//            if (TextUtils.isEmpty(password)) {
//                Toast.makeText(applicationContext, "password is required", Toast.LENGTH_SHORT)
//                    .show()
//            }
//
//            if (TextUtils.isEmpty(confirmPassword)) {
//                Toast.makeText(
//                    applicationContext,
//                    "confirm password is required",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//
//            if (!password.equals(confirmPassword)) {
//                Toast.makeText(applicationContext, "password not match", Toast.LENGTH_SHORT).show()
//            }
            registerUser(userName, email, password)

        }

        btnLogin.setOnClickListener {
            val intent = Intent(
                this@SignUpActivity,
                LoginActivity::class.java
            )
            startActivity(intent)
            finish()
        }
    }

    private fun registerUser(userName: String, email: String, password: String) {
        var email = "nayana@gmail.com"
        var password = "789456"

        auth.createUserWithEmailAndPassword(email, password)
            .addOnFailureListener {
                Toast.makeText(this,it.message,Toast.LENGTH_SHORT).show()
                Log.d("TAG", "registerUser: " + it)
            }
            .addOnCompleteListener(this) {
                if (it.isSuccessful) {
                    val user: FirebaseUser? = auth.currentUser
                    val userId: String = user!!.uid

                    databaseReference =
                        FirebaseDatabase.getInstance().getReference("Users").child(userId)

                    val hashMap: HashMap<String, String> = HashMap()
                    hashMap.put("userId", userId)
                    hashMap.put("userName", "nayana")
                    hashMap.put("userType", "counsellor")
                    hashMap.put("email", email)
                    hashMap.put("profileImage", "")
                    hashMap.put("org_id", "3333")

                    Log.d("TAG", "registerUser: " + hashMap)

//                    databaseReference.updateChildren(hashMap as Map<String, Any>)

                    databaseReference.setValue(hashMap).addOnFailureListener {
                        Log.d("TAG", "registerUser: " + it)
                    }.addOnCompleteListener(this) {
                        if (it.isSuccessful) {
                            Log.d("TAG", "registerUser: " + "SUCCESS")
                            //open home activity
                            etName.setText("")
                            etEmail.setText("")
                            etPassword.setText("")
                            etConfirmPassword.setText("")
                            val intent = Intent(
                                this@SignUpActivity,
                                UsersActivity::class.java
                            )
                            startActivity(intent)
                            finish()
                        }
                    }
                }
            }
    }
}