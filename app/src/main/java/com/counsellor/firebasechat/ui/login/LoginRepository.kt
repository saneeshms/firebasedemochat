package com.skysys.plugh.ui.login

import com.skysys.plugh.network.CounsellorApi
import com.skysys.plugh.network.SafeApiCall
import javax.inject.Inject

class LoginRepository @Inject constructor(var plughApi: CounsellorApi) {

    suspend fun Login(reqBody: HashMap<String, String>) = plughApi.userLogin(reqBody)

    suspend fun userLogout(reqBody: HashMap<String, String>) = plughApi.userLogout(reqBody)



}
