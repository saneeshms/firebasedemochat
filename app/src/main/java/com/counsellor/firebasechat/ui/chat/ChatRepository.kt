package com.skysys.plugh.eap.chat

import com.skysys.plugh.network.CounsellorApi
import javax.inject.Inject

class ChatRepository @Inject constructor(val api: CounsellorApi) {

    suspend fun fetchUsersList(reqBody : HashMap<String,String>) = api.fetchUsersList(reqBody)

    suspend fun saveCounsellingData(reqBody : HashMap<String,String>) = api.saveCounsellingData(reqBody)

}