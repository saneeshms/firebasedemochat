package com.counsellor.firebasechat.ui.chat

import android.Manifest.permission.RECORD_AUDIO
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.counsellor.firebasechat.Constants.Constants
import com.counsellor.firebasechat.RetrofitInstance
import com.counsellor.firebasechat.adapter.ChatAdapter
import com.counsellor.firebasechat.databinding.ActivityChatBinding
import com.counsellor.firebasechat.model.ChatUser
import com.counsellor.firebasechat.model.NotificationData
import com.counsellor.firebasechat.model.PushNotification
import com.skysys.plugh.common.BaseActivity
import com.skysys.plugh.eap.chat.ChatViewModel
import com.skysys.plugh.eap.model.Chat
import com.skysys.plugh.utils.PreferenceManager
import com.skysys.plugh.utils.Util
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class ChatActivity : BaseActivity() {

    var firebaseUser: FirebaseUser? = null
    var chatList = ArrayList<Chat>()
    var topic = ""
    var mediaRecorder: MediaRecorder? = null
    lateinit var outputPath: String
    val requestPermissionCode = 1
    var isRecording: Boolean = false
    lateinit var chatAdapter: ChatAdapter
    lateinit var bindings: ActivityChatBinding

    lateinit var referenceChat: DatabaseReference
    lateinit var chatId: String

    lateinit var chatUserData: ChatUser

    @Inject
    lateinit var viewModel: ChatViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindings = ActivityChatBinding.inflate(layoutInflater)
        setContentView(bindings.root)

        initViews()
        initControls()
        readChatData()

    }

    private fun readChatData() {
        readMessage(firebaseUser!!.uid, chatUserData.firebaseUserId!!)
    }

    override fun onResume() {
        super.onResume()
        PreferenceManager.setOnChatScreen(true)
        Util.stopRingTone()
    }

    override fun onPause() {
        super.onPause()
        PreferenceManager.setOnChatScreen(false)
    }

    private fun initViews() {

        if (Build.VERSION.SDK_INT >= 11 && chatList.size > 0) {
            bindings.chatRecyclerView.addOnLayoutChangeListener { p0, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                if (bottom < oldBottom) {
                    bindings.chatRecyclerView.postDelayed(Runnable {
                        bindings.chatRecyclerView.smoothScrollToPosition(
                            bindings.chatRecyclerView.adapter!!.itemCount - 1
                        )
                    }, 100)
                }
            }
        }

        chatRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        chatUserData = intent.getParcelableExtra<ChatUser>("userData")!!
        chatId = chatUserData.chatId!!

        Log.d(Constants.TAG_CHECK, "chatId : $chatId")
        referenceChat = FirebaseDatabase.getInstance().reference.child("Chat").child(chatId)

        firebaseUser = FirebaseAuth.getInstance().currentUser

        if (chatUserData.firstname != null)
            bindings.tvUserName.text = chatUserData.firstname

        if (chatUserData.isChatEnabled.contentEquals("false")) {
            bindings.r1.visibility = View.GONE
            bindings.txtChatEnd.visibility = View.GONE
        }

        bindings.etMessage.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                Log.d(Constants.TAG_CHECK, "hasFocus: $hasFocus")
            } else {
                Log.d(Constants.TAG_CHECK, "hasFocus: $hasFocus")
            }
        })
    }

    private fun initControls() {

        bindings.aviLoading.show()

        chatAdapter = ChatAdapter(this@ChatActivity, chatList)
        chatRecyclerView.adapter = chatAdapter

        imgBack.setOnClickListener {
            onBackPressed()
        }

        txtChatEnd.setOnClickListener {

            var reqData: HashMap<String, String> = hashMapOf()
            reqData["user_id"] = chatUserData.userId!!
            reqData["isChatEnabled"] = "false"
            reqData["chat_id"] = chatId
            reqData["created_at"] = Util.getDateTime()
            reqData["firebase_userid"] = chatUserData.firebaseUserId!!

            reqData["firebase_counsellor_id"] = firebaseUser!!.uid
            reqData["counsellor_id"] = PreferenceManager.getUserId()

            viewModel.saveCounsellingData(reqData)
            viewModel._status.observe(this) {
                showToast(this, it)
                finish()
            }
        }
//
//        reference!!.addValueEventListener(object : ValueEventListener {
//            override fun onCancelled(error: DatabaseError) {
//
//            }
//
//            override fun onDataChange(snapshot: DataSnapshot) {
//
//                val user = snapshot.getValue(ChatUser::class.java)
//                if (user?.firstname != null)
//                    tvUserName.text = user!!.firstname
//                if (user?.profileImage != null && user!!.profileImage == "") {
//                    imgProfile.setImageResource(R.drawable.profile_image)
//                } else {
////                    Glide.with(this@ChatActivity).load(user.profileImage).into(imgProfile)
//                }
//            }
//        })

        btnSendMessage.setOnClickListener {

            var message: String = etMessage.text.toString()

            if (message.isEmpty()) {
                Toast.makeText(applicationContext, "message is empty", Toast.LENGTH_SHORT).show()
                etMessage.setText("")
            } else {

                sendMessage(firebaseUser!!.uid, chatUserData.firebaseUserId!!, message)

                etMessage.setText("")

                topic = "/topics/"+chatUserData.firebaseUserId!!
                var userName = ""
                if (chatUserData.firstname != null)
                    userName = chatUserData.firstname!!


                PushNotification(
                    NotificationData("PlugH Counsellor", message,PreferenceManager.getUserName()
                        , chatUserData.chatId!!,firebaseUser!!.uid!!, PreferenceManager.getUserId(),
                    ),
                    topic
                ).also {
                    sendNotification(it)
                }

            }
        }
//
//        btnVoiceBtn.setOnClickListener {
//
//            if(checkPermission()) {
//
//                if(!isRecording) {
//                    isRecording = true
//                    outputPath = this.getExternalFilesDir(null)!!.absolutePath+"/" + System.currentTimeMillis() + ".3gp";
//                    mediaRecorderReady()
//                    mediaRecorder!!.prepare()
//                    mediaRecorder!!.start()
//                    Log.d("AUDIOCHAT", " mediaRecorder!!.start(): ")
//                }
//                else{
//                    isRecording = false
//                    mediaRecorder!!.stop()
//                    mediaRecorder!!.release()
//                    mediaRecorder = null
//                    Log.d("AUDIOCHAT", " mediaRecorder!!.release(): ")
//                }
//            }
//            else
//            {
//                requestPermission()
//            }
//
//        }


    }

//
//    fun mediaRecorderReady() {
//        mediaRecorder = MediaRecorder()
//        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
//        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
//        mediaRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
//        mediaRecorder!!.setOutputFile(outputPath)
//    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(WRITE_EXTERNAL_STORAGE, RECORD_AUDIO),
            requestPermissionCode
        )
    }

    private fun checkPermission(): Boolean {
        var result = ContextCompat.checkSelfPermission(applicationContext, WRITE_EXTERNAL_STORAGE)
        var result1 = ContextCompat.checkSelfPermission(applicationContext, RECORD_AUDIO)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED

    }

    private fun sendMessage(senderId: String, receiverId: String, message: String) {

        var hashMap: HashMap<String, String> = HashMap()
        hashMap.put("senderId", senderId)
        hashMap.put("receiverId", receiverId)
        hashMap.put("message", message)

        Log.d(Constants.TAG_CHECK, "sendMessage: " + hashMap)
        referenceChat!!.push().setValue(hashMap)

    }

    private fun uploadAudio() {
        if (outputPath != null) {

            var storageRef: StorageReference = FirebaseStorage.getInstance().reference
            var reference: DatabaseReference? = FirebaseDatabase.getInstance().reference

            var ref: StorageReference = storageRef.child("image/" + UUID.randomUUID().toString())
            ref.putFile(Uri.fromFile(File(outputPath)))
                .addOnSuccessListener {

                    ref.downloadUrl.addOnSuccessListener {

                        val hashMap: HashMap<String, String> = HashMap()
                        hashMap.put("senderId", "1111")
                        hashMap.put("receiverId", "2222")
                        hashMap.put("message", "test")
                        hashMap.put("audioFile", it.toString())
                        reference!!.child("Chat").child("123456").push().setValue(hashMap)

//                        progressBar.visibility = View.GONE
                        Toast.makeText(applicationContext, "Uploaded", Toast.LENGTH_SHORT).show()
//                        btnSave.visibility = View.GONE
                    }

                }
                .addOnFailureListener {
//                    progressBar.visibility = View.GONE
                    Toast.makeText(applicationContext, "Failed" + it.message, Toast.LENGTH_SHORT)
                        .show()

                }

        }
    }

    fun readMessage(senderId: String, receiverId: String) {
        val databaseReference: DatabaseReference =
            FirebaseDatabase.getInstance().getReference("Chat").child(chatId)

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {

                aviLoading.hide()
                chatList.clear()

                for (dataSnapShot: DataSnapshot in snapshot.children) {
                    val chat = dataSnapShot.getValue(Chat::class.java)
                    Log.d(Constants.TAG_CHECK, "onDataChange: " + chat!!.message)

                    if (chat!!.senderId.equals(senderId) && chat!!.receiverId.equals(receiverId) ||
                        chat!!.senderId.equals(receiverId) && chat!!.receiverId.equals(senderId)
                    ) {
                        chatList.add(chat)
                    }
                }
                chatAdapter.update(chatList)
                if (chatList.size > 0)
                    bindings.chatRecyclerView.smoothScrollToPosition(chatList.size - 1)

            }
        })
    }

    private fun sendNotification(notification: PushNotification) =
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = RetrofitInstance.api.postNotification(notification)
                if (response.isSuccessful) {
                    Log.d(Constants.TAG_CHECK, "Response")
                } else {
                    Log.e(Constants.TAG_CHECK, response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e(Constants.TAG_CHECK, e.toString())
            }
        }


}