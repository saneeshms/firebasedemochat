package com.skysys.plugh.eap.chat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.counsellor.firebasechat.model.ChatUser
import com.skysys.plugh.common.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class ChatViewModel @Inject constructor(var respoChatRepository: ChatRepository) : BaseViewModel() {

    var userListData : MutableLiveData<ArrayList<ChatUser>> = MutableLiveData()
    val _userListData : LiveData<ArrayList<ChatUser>>
    get() = userListData

    fun fetchUsersList(reqData : HashMap<String,String>)
    {
        loading.value = true
        viewModelScope.launch {
            var responseData = respoChatRepository.fetchUsersList(reqData).body()
            if(responseData == null)
                status.value = "Poor network. Please try again !"
            else if(responseData!!.code.contentEquals("200"))
                userListData.value = responseData.data!! as ArrayList<ChatUser>
            else
                status.value = responseData.status
            loading.value = false
        }
    }

    fun saveCounsellingData(reqData : HashMap<String,String>)
    {
        loading.value = true
        viewModelScope.launch {
            var responseData = respoChatRepository.saveCounsellingData(reqData).body()
            if(responseData == null)
                status.value = "Poor network. Please try again !"
            else if(responseData!!.code.contentEquals("200"))
                status.value = responseData.status!!
            else
                status.value = responseData.status
            loading.value = false
        }
    }

}