package com.counsellor.firebasechat.ui

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.messaging.FirebaseMessaging
import com.counsellor.firebasechat.Constants.Constants
import com.counsellor.firebasechat.CustomViewPagerAdapter
import com.counsellor.firebasechat.R
import com.counsellor.firebasechat.ui.login.LoginActivity
import com.skysys.plugh.ui.login.LoginViewModel
import com.skysys.plugh.utils.PreferenceManager
import com.skysys.plugh.utils.Util
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_conversation.*
import kotlinx.android.synthetic.main.layout_logout.*
import javax.inject.Inject

@AndroidEntryPoint
class ConversationActivity : AppCompatActivity() {

    private lateinit var adapter: CustomViewPagerAdapter
    var viewPager: ViewPager? = null

    @Inject
    lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_conversation)
        Util.mContext = applicationContext

        viewPager = findViewById<ViewPager>(R.id.viewpager)
        val tabLayout = findViewById<TabLayout>(R.id.tab)

        PreferenceManager.setOnChatScreen(false)

        adapter = CustomViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(ActiveFragment(), "Active Chats")
        adapter.addFragment(HistoryFragment(), "Previous Chats")

        val firebase: FirebaseUser = FirebaseAuth.getInstance().currentUser!!
        var userid = firebase.uid
        FirebaseMessaging.getInstance().subscribeToTopic("/topics/$userid")

        viewPager?.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)

        viewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
//                    0 ->
//                    1 ->
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        txtlogout.setOnClickListener {
            setLogoutDialog()
        }

        loginViewModel._status.observe(this) {
            PreferenceManager.setUserId("")
            PreferenceManager.setToken("")

            startActivity(
                Intent(this, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            )
            finish()
        }

    }


    private fun setLogoutDialog() {

        var view = LayoutInflater.from(this).inflate(R.layout.layout_logout, null)
        var dialog = this?.let { Dialog(it) }
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog!!.setContentView(view)
        dialog.show()
        dialog.btnBack.setOnClickListener {
            dialog.dismiss()
        }
        dialog.btnLogout.setOnClickListener {
            userLogout()
            dialog.dismiss()
        }

    }

    private fun userLogout() {

        val hashMap: HashMap<String, String> = hashMapOf()
        hashMap.put("user_id", PreferenceManager.getUserId())
        hashMap.put("flag", "2")

        Log.d(Constants.TAG_CHECK, "userLogout : " + hashMap)
        loginViewModel.userLogout(hashMap, this)
    }


}