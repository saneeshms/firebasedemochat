package com.counsellor.firebasechat.ui.chat

import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging
import com.counsellor.firebasechat.Constants.Constants
import com.counsellor.firebasechat.adapter.UserAdapter
import com.counsellor.firebasechat.databinding.ActivityUsersBinding
import com.counsellor.firebasechat.firebase.FirebaseService
import com.counsellor.firebasechat.model.ChatUser
import com.counsellor.firebasechat.model.Counsellor
import com.skysys.plugh.eap.chat.ChatViewModel
import com.skysys.plugh.utils.PreferenceManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_users.*
import javax.inject.Inject

@AndroidEntryPoint
class UsersActivity : AppCompatActivity() {

    var userList = ArrayList<ChatUser>()

    @Inject
    lateinit var viewModel: ChatViewModel
    lateinit var binding: ActivityUsersBinding
    lateinit var userAdapter: UserAdapter
    lateinit var counsellor: Counsellor
    lateinit var useridFireBase: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUsersBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews()
        initControls()
//        getUsersList()

    }

    private fun initControls() {

        val firebase: FirebaseUser = FirebaseAuth.getInstance().currentUser!!
        useridFireBase = firebase.uid

        var reqdata: HashMap<String, String> = hashMapOf()
        reqdata["user_id"] = PreferenceManager.getUserId()
        reqdata["list_type"] = "history"
        viewModel.fetchUsersList(reqdata)

        viewModel._userListData.observe(this) {

            userList = it as ArrayList<ChatUser>
            userAdapter.update(userList)

        }


    }

    private fun initViews() {

        userAdapter = UserAdapter(this@UsersActivity, userList)
        userRecyclerView.adapter = userAdapter

        FirebaseService.sharedPref = getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            FirebaseService.token = task.result
        })

        userRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

    }

    fun getUsersList() {

        val firebase: FirebaseUser = FirebaseAuth.getInstance().currentUser!!
        var useridFireBase = firebase.uid
        FirebaseMessaging.getInstance().subscribeToTopic("/topics/$useridFireBase")

        val databaseReference: DatabaseReference =
            FirebaseDatabase.getInstance().getReference("Users")
//        val dbReference : DatabaseReference = databaseReference.child("org_id")
//        dbReference.addListenerForSingleValueEvent(valueEventListener)
        Log.d(Constants.TAG_CHECK, "useridFireBase: " + useridFireBase)

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()
//                val currentUser = snapshot.getValue(User::class.java)
//                if (currentUser!!.profileImage == ""){
//                    imgProfile.setImageResource(R.drawable.profile_image)
//                }else{
//                    Glide.with(this@UsersActivity).load(currentUser.profileImage).into(imgProfile)
//                }

                for (dataSnapShot: DataSnapshot in snapshot.children) {
                    val user = dataSnapShot.getValue(ChatUser::class.java)

                    Log.d(Constants.TAG_CHECK, "user!!.org_id: " + user!!.userId)
                    if (user!!.userId != useridFireBase) {
                        userList.add(user!!)
                    }
                }

            }

        })
    }

    var valueEventListener: ValueEventListener = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            if (dataSnapshot.child("org_id").getValue(String::class.java) == "1111") {
                Log.d("WEWEW", "onDataChange: ")
            } else if (dataSnapshot.child("userType")
                    .getValue(String::class.java) == "Venue Owner"
            ) {

            }
        }

        override fun onCancelled(databaseError: DatabaseError) {

        }
    }
}