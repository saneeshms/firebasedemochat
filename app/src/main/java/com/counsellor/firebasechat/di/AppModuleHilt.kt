package com.skysys.plugh.di

import android.content.Context
import com.skysys.plugh.network.CounsellorApi
import com.skysys.plugh.network.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModuleHilt {

    @Provides
    fun provideAuthApi(
        @ApplicationContext context: Context,
        remoteDataSource: RemoteDataSource,
    ): CounsellorApi {
        return remoteDataSource.buildApi(CounsellorApi::class.java)
    }
//
//    @Provides
//    @Singleton
//    fun dataStorePreference(@ApplicationContext context: Context): DataStore<Preferences> =
//        context.createDataStore("settings")

}