package com.skysys.plugh.network

import android.content.Context
import com.skysys.plugh.utils.PreferenceManager
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject


class RemoteDataSource @Inject constructor() {

    companion object {
//        private const val BASE_URL = "http://plugapi.skysysonline.com/api/"
//        private const val BASE_URL = "http://plugh.co.in/plug/api/"                               //prod
        private const val BASE_URL = "http://plugh.co.in/TEST/plug/api/"                                 //test
    }

    fun <Api> buildApi(
        api: Class<Api>,
    ): Api {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(getRetrofitClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(api)
    }

    private fun getRetrofitClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor { chain ->
                chain.proceed(chain.request().newBuilder()
                    .addHeader("Authorization","Bearer "+PreferenceManager.getToken())
                    .build())
            }.also { client ->
                val logging = HttpLoggingInterceptor()
                logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                client.addInterceptor(logging)
            }.build()
    }
}