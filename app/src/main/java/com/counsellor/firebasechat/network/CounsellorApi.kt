package com.skysys.plugh.network

import com.counsellor.firebasechat.model.ChatUser
import com.counsellor.firebasechat.model.ResponseResult
import com.counsellor.firebasechat.model.User
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface CounsellorApi {

    @FormUrlEncoded
    @POST("counsellorLogin")
    suspend fun userLogin(@FieldMap appdata: HashMap<String, String>): Response<ResponseResult<User>>

    @FormUrlEncoded
    @POST("logout")
    suspend fun userLogout(@FieldMap appdata: HashMap<String, String>): Response<ResponseResult<User>>

    @FormUrlEncoded
    @POST("fetchUsersList")
    suspend fun fetchUsersList(@FieldMap appdata: HashMap<String, String>): Response<ResponseResult<ArrayList<ChatUser>>>

    @FormUrlEncoded
    @POST("saveCounsellingData")
    suspend fun saveCounsellingData(@FieldMap appdata: HashMap<String, String>): Response<ResponseResult<ChatUser>>

    @FormUrlEncoded
    @POST("logout")
    suspend fun logout(@FieldMap appdata: HashMap<String, String>): Response<ResponseResult<String>>


}